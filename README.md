<p align="center">
  <img src="https://storage.googleapis.com/gweb-uniblog-publish-prod/images/In-lineImage_1500x850.max-1000x1000.jpg" /> 
</p>

# Nexus 6P Cherry-Picks #

### android_art:

```bash
# Initialize remote
git remote add lineage https://github.com/LineageOS/android_art

# Fetch remote
git fetch lineage

# Cherry-pick commit
git cherry-pick 297051474fa7d3315f5b560d1d3b5fd55d6fc5f3
```

### android_hardware_qcom_display:

```bash
# Initialize remote
git remote add statix https://github.com/StatiXOS/android_hardware_qcom_display

# Fetch remote
git fetch statix

# Cherry-pick commit
git cherry-pick 9ff7bc637d9952e458a92d1c39b87113d054e641
git cherry-pick 5f3a798b5e751d587fa8e3a70a0c8cea8a6ce031
```

### android_hardware_qcom_gps:

```bash
# Initialize remote
git remote add dust https://github.com/PixelDust-Project-X/android_hardware_qcom_gps

# Fetch remote
git fetch dust

# Cherry-pick commit
git cherry-pick 533aeabda5d9452448cd4ef94d3f8896a683ea0a
git cherry-pick 7b39b53b9251c2a9c0f81b6ca6fa4060e34bb7a3
```

### android_hardware_qcom_media:

```bash
# Initialize remote
git remote add lineage https://github.com/LineageOS/android_hardware_qcom_media

# Fetch remote
git fetch lineage

# Cherry-pick commit
git cherry-pick 261987cd6d779fa7ca814ee5d3df2241206063c1
```

### android_frameworks_native:

```bash
# Initialize remote
git remote add pixelboot https://gitlab.com/PixelBoot/android_frameworks_native

# Fetch remote
git fetch pixelboot

# Cherry-pick commit
git cherry-pick 726a4a0283a6a851aa61353da3f7c9a0dfc1b3c7
```